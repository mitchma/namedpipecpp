#include <iostream>
#include <windows.h>

int main()
{
	// open pipe
	printf("Attempting to connect to pipe\n");
	HANDLE pipe = CreateFile(
		"\\\\.\\pipe\\mypipe",
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	if (pipe == INVALID_HANDLE_VALUE)
	{
		printf("Failed to connect to pipe: %lu\n", GetLastError());
		system("pause");
		return 1;
	}

	while (true)
	{
		printf("Reading from pipe\n");

		// blocks until there is data to read
		wchar_t buffer[128];

		DWORD numBytesRead = 0;
		int result = ReadFile(
			pipe,
			buffer,
			127 * sizeof(wchar_t),
			&numBytesRead,
			NULL // no overlapped IO
		);

		if (result)
		{
			buffer[numBytesRead / sizeof(wchar_t)] = '\0';
			printf("Number of bytes sent: %d\n", numBytesRead);

			size_t bufferSize = wcslen(buffer) + 1;
			const size_t newsize = 100;
			size_t convertedChars = 0;
			char nstring[newsize];
			wcstombs_s(&convertedChars, nstring, bufferSize, buffer, _TRUNCATE);
			printf("%s\n", nstring);
		}
		else
		{
			printf("Failed to read from pipe\n");
		}
		Sleep(1000);
	}

	// finish
	CloseHandle(pipe);
	printf("Done\n");
	system("pause");
	return 0;
}
