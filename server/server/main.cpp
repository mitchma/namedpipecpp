#include <iostream>
#include <windows.h>

// Write data to pipe.
void writeData(HANDLE pipe, const wchar_t *data)
{
	printf("Sending data to pipe\n");

	DWORD numBytesWritten = 0;
	int result = WriteFile(
		pipe,
		data,
		wcslen(data) * sizeof(wchar_t),
		&numBytesWritten,
		NULL // no overlapped IO
	);

	result ? printf("Number of bytes sent: %d\n", numBytesWritten) : printf("Failed to send data: %lu", GetLastError());
	Sleep(1000);
}

int main()
{
	printf("Creating named pipe\n");

	// create pipe
	HANDLE pipe = CreateNamedPipe(
		TEXT("\\\\.\\pipe\\mypipe"), // pipe name
		PIPE_ACCESS_DUPLEX, // 2-way pipe
		PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
		1, // allow one instance of pipe
		0, // no outbound buffer
		0, // no inbound buffer
		0, // use default wait time
		NULL // default security attributes
	);

	if (pipe == NULL || pipe == INVALID_HANDLE_VALUE)
	{
		printf("Failed to create pipe: %lu\n", GetLastError());
		system("pause");
		return 1;
	}

	printf("Waiting for a client\n");

	// blocks until client connected
	int result = ConnectNamedPipe(pipe, NULL);
	if (result == 0)
	{
		printf("Connection failed: %lu\n", GetLastError());
		CloseHandle(pipe);
		system("pause");
		return 1;
	}

	writeData(pipe, L"This is sentence 1.");
	writeData(pipe, L"This is sentence 2.");
	writeData(pipe, L"This is sentence 3.");

	// close pipe
	CloseHandle(pipe);
	printf("Pipe has closed\n");
	system("pause");
	return 0;
}
