
# NamedPipeCPP #

Playing around with Windows named pipes in C++. The project consists of the pipe server and client. The server is programmed to write three generic sentences to the pipe upon client connection. Simply run the server exe and then the client exe files provided in the zip.
